TARGET=hello
obj-m += $(TARGET).o
LINUX_VERSION = linux_6_5

build:
	make -C $(shell nix-build -E '(import <nixpkgs> {}).$(LINUX_VERSION).dev' --no-out-link)/lib/modules/*/build M=$(shell pwd) modules
	gcc -o set_mode set_mode.c

ins: build
	sudo insmod $(TARGET).ko

rm:
	sudo rmmod $(TARGET).ko

clean:
	rm Module.symvers
	rm modules.order
	rm $(TARGET).o
	rm $(TARGET).ko
	rm $(TARGET).mod
	rm $(TARGET).mod.c
	rm $(TARGET).mod.o
	rm .modules.order.cmd
	rm .Module.symvers.cmd
	rm .$(TARGET).ko.cmd
	rm .$(TARGET).mod.cmd
	rm .$(TARGET).mod.o.cmd
	rm .$(TARGET).o.cmd

dmesg:
	dmesg --ctime --color=auto --follow

test:
	./full_test.sh
