Just a toy module for a char device driver to get familiar with linux kernel development.

Everything that is sent to the char device can be echoed in one of three modes:
- Passthrough: Echo the input 1:1
- TO_UPPER: Convert input to uppercase before echo
- to_lower: Convert input to lowercase before echo

The mode is set via ioctl from "set_mode".

To see it in action run `make test`.
