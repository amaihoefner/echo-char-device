#!/usr/bin/env bash

TEST_STRING="${1:-HELLO, world!}"
CHAR_FILE="./hello_char"

make ins >> /dev/null
sudo mknod $CHAR_FILE c "$(awk '{if ($2 == "hello") print $1}' /proc/devices)" 0
sudo chown 1000:1000 $CHAR_FILE

echo "Test string is:"
echo "$TEST_STRING"
echo "$TEST_STRING" >> $CHAR_FILE
echo ""

echo "Passthrough mode:"
cat $CHAR_FILE
echo ""

echo "TO_UPPER mode:"
./set_mode $CHAR_FILE 1
cat $CHAR_FILE
echo ""

echo "to_lower mode:"
./set_mode $CHAR_FILE 2
cat $CHAR_FILE
echo ""

make rm >> /dev/null
make clean >> /dev/null
rm $CHAR_FILE
