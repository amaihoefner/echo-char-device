#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.h"

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("\tArgument 1 (required): char device (string)\n");
    printf("\t\tPath to char device file\n");

    printf("\tArgument 2 (optional): mode (int)\n");
    printf("\t\t0 -> Passthrough\n");
    printf("\t\t1 -> TO_UPPER\n");
    printf("\t\t2 -> to_lower\n");
    printf("\t\tother -> undefined\n");
  }

  int fd = open(argv[1], O_RDWR);
  if (fd < 0) {
    printf("Failed to open device file\n");
    return 0;
  }

  if (argv[2]) {
    int32_t mode = atoi(argv[2]);
    ioctl(fd, HELLO_SET_MODE, (int32_t *)&mode);
  } else {
    int32_t mode;
    ioctl(fd, HELLO_GET_MODE, (int32_t *)&mode);
    printf("Mode: %d", mode);
  }

  close(fd);
}
