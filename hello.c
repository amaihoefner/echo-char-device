#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/ioctl.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#include "common.h"

#define BUFFER_SIZE 1024

MODULE_LICENSE("MIT");

static int major_number;
static char char_device_buffer[BUFFER_SIZE];
static int char_device_open = 0;

static inline char to_upper(char c) { return c - 32; }
static inline char to_lower(char c) { return c + 32; }

enum Mode {
  PASSTHROUGH = 0,
  TO_UPPER = 1,
  TO_LOWER = 2,
};
static enum Mode mode = PASSTHROUGH;

long hello_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
  switch (cmd) {
  case HELLO_SET_MODE:
    if (copy_from_user(&mode, (int32_t *)arg, sizeof(mode))) {
      pr_err("Failed set mode\n");
      printk(KERN_ERR "hi");
      return -EFAULT;
    }
    pr_info("Mode = %d\n", mode);
    break;
  case HELLO_GET_MODE:
    if (copy_to_user((int32_t *)arg, &mode, sizeof(mode))) {
      pr_err("Failed to get mode\n");
      return -EFAULT;
    }
    pr_info("Mode = %d\n", mode);
    break;
  default:
    pr_info("Unknown command\n");
    break;
  }
  return 0;
}

static ssize_t hello_read(struct file *file, char *buf, size_t count,
                          loff_t *ppos) {
  ssize_t bytes_read = 0;

  while (count && (char_device_buffer[*ppos] != '\0')) {
    char next_char = char_device_buffer[(*ppos)++];

    switch (mode) {
    case TO_UPPER: {
      bool char_is_lowercase = next_char >= 'a' && next_char <= 'z';
      if (char_is_lowercase) {
        char next_char_uppercase = to_upper(next_char);
        put_user(next_char_uppercase, buf++);
      } else {
        put_user(next_char, buf++);
      }
      break;
    }

    case TO_LOWER: {
      bool char_is_uppercase = next_char >= 'A' && next_char <= 'Z';
      if (char_is_uppercase) {
        char next_char_lowercase = to_lower(next_char);
        put_user(next_char_lowercase, buf++);
      } else {
        put_user(next_char, buf++);
      }
      break;
    }
    default:
    case PASSTHROUGH:
      put_user(next_char, buf++);
      break;
    }

    count--;
    bytes_read++;
  }

  return bytes_read;
}

static ssize_t hello_write(struct file *file, const char *buf, size_t count,
                           loff_t *ppos) {
  ssize_t bytes_written = 0;

  if (count < BUFFER_SIZE - 1) {
    while (bytes_written < count) {
      get_user(char_device_buffer[*ppos], buf++);
      (*ppos)++;
      count--;
      bytes_written++;
    }
  }

  char_device_buffer[*ppos] = '\0';

  return bytes_written;
}

static int hello_open(struct inode *inode, struct file *filp) {
  if (char_device_open)
    return -EBUSY;

  char_device_open++;
  return 0;
}

static int hello_release(struct inode *inode, struct file *filp) {
  char_device_open--;
  return 0;
}

static struct file_operations hello_fops = {
    .owner = THIS_MODULE,
    .read = hello_read,
    .write = hello_write,
    .open = hello_open,
    .release = hello_release,
    .unlocked_ioctl = hello_ioctl,
};

static int __init hello_init(void) {
  pr_info("Starting the hello module\n");

  major_number = register_chrdev(0, THIS_MODULE->name, &hello_fops);

  if (major_number < 0) {
    pr_err("Failed to register a major number\n");
    return major_number;
  }

  pr_info("Registered with major number %d\n", major_number);

  return 0;
}

static void __exit hello_exit(void) {
  pr_info("Removing the hello module\n");
  unregister_chrdev(major_number, THIS_MODULE->name);
}

module_init(hello_init);
module_exit(hello_exit);
